from copy import copy, deepcopy
from heapq import heappop, heappush
from typing import Dict, List, Set, Tuple, Union
from scipy.optimize import linprog

from src.custom_types import (
    DeltaGamma,
    Gamma,
    GammaBar,
    GammaPlus,
    PSEUDO_INFINITY,
    GammaBarOptions,
)
from src.delay import convert_to_fixed_delay, is_delay_controllable
from src.stnu import ContingentLink, Edge, STNU


Constraint = Tuple[List[float], float]


class CarGammaBarOptions(GammaBarOptions):
    def __init__(self, worker_type=1):
        self.worker_type = worker_type
        self.num_popped = 0
        self.options: List[Tuple[float, Tuple[GammaPlus, DeltaGamma]]] = []
        if worker_type == 1:
            self.options = [
                (1.0, (PSEUDO_INFINITY, PSEUDO_INFINITY)),
                (0.99, (0, 0)),
            ]
        elif worker_type == 2:
            self.options = [
                (1.0, (PSEUDO_INFINITY, PSEUDO_INFINITY)),
                (0.99, (15, 0)),
            ]
        elif worker_type == 3:
            self.options = [
                (1.0, (30, 30)),
                (0.95, (15, 15)),
                (0.90, (15, 0)),
                (0.05, (0, 0)),
            ]


def pick_highest_prob(car_gamma: Dict[str, CarGammaBarOptions]) -> GammaBar:
    gb = {}
    for k, v in car_gamma.items():
        gb[k] = v.best_gamma()
    return gb


def orig_weight(edge: Edge, gamma_bar: GammaBar) -> float:
    weight = edge.weight

    for k, v in edge.annotations.items():
        tuple_index = 0
        if k.startswith("gamma_plus_"):
            node = k[len("gamma_plus_") :]
        elif k.startswith("delta_gamma_"):
            node = k[len("delta_gamma_") :]
            tuple_index = 1
        else:
            continue

        weight -= v * gamma_bar[node][tuple_index]

    return weight


def candidate_assignment(
    variable_mapping: Dict[str, int], gamma_bar: GammaBar
) -> List[float]:
    x = [0.0 for _ in variable_mapping.keys()]
    for k, v in variable_mapping.items():
        tuple_index = 0
        if k.startswith("gamma_plus_"):
            node = k[len("gamma_plus_") :]
        elif k.startswith("delta_gamma_"):
            node = k[len("delta_gamma_") :]
            tuple_index = 1
        else:
            continue
        x[v] = gamma_bar[node][tuple_index]
    return x


def extract_resolutions(
    edges: List[Edge],
    variable_mapping: Dict[str, int],
    stnu: STNU,
    candidate_gamma: Gamma,
    gamma_bar: GammaBar,
) -> List[Union[Constraint, List[Constraint]]]:
    resolutions = []
    candidate = candidate_assignment(variable_mapping, gamma_bar)
    freq_list: List[float] = [0] * len(variable_mapping)
    total_weight = 0
    for e in edges:
        total_weight += orig_weight(e, gamma_bar)
        for k, v in e.annotations.items():
            freq_list[variable_mapping[k]] += v

    total = total_weight
    num_greater_than_zero = 0
    for i, val in enumerate(freq_list):
        total += val * candidate[i]
        if val > 0:
            num_greater_than_zero += 1

    if total < 0 < num_greater_than_zero:
        resolutions.append((freq_list, total_weight))

    lower_indices = [
        i
        for i in range(len(edges))
        if edges[i].label is not None and edges[i].label.islower()
    ]
    for i in lower_indices:
        edge = edges[i]
        lower_label = edge.label.upper()
        link = [
            l
            for l in stnu.contingent_links
            if l.end == lower_label and isinstance(l, ContingentLink)
        ][0]
        gamma_plus_index = variable_mapping["gamma_plus_%s" % lower_label]
        delta_gamma_index = variable_mapping["delta_gamma_%s" % lower_label]
        if candidate_gamma[lower_label] == PSEUDO_INFINITY:
            l1_freq = [0] * len(variable_mapping)
            l1_freq[gamma_plus_index] = -1
            l1 = PSEUDO_INFINITY - candidate[gamma_plus_index] - 1

            l2_freq = [0] * len(variable_mapping)
            l2_freq[delta_gamma_index] = -1
            l2 = (
                link.max_duration() - link.min_duration() - candidate[delta_gamma_index]
            )

            if l1 < 0 and l2 < 0:
                resolutions.append(
                    [
                        (l1_freq, PSEUDO_INFINITY - 1),
                        (l2_freq, link.max_duration() - link.min_duration()),
                    ]
                )
            elif l1 < 0:
                resolutions.append((l1_freq, PSEUDO_INFINITY - 1))
            elif l2 < 0:
                resolutions.append((l2_freq, link.max_duration() - link.min_duration()))
        else:
            next_index = (i + 1) % len(edges)
            subpath_edges = [edge, edges[next_index]]
            total_weight = edge.weight + edges[next_index].weight
            while total_weight > candidate_gamma[lower_label]:
                next_index = (next_index + 1) % len(edges)
                subpath_edges.append(edges[next_index])
                total_weight += edges[next_index].weight

            orig = 0
            annotation_list: List[float] = [0] * len(variable_mapping)
            for s in subpath_edges:
                orig += orig_weight(s, gamma_bar)
                for k, v in s.annotations.items():
                    annotation_list[variable_mapping[k]] += v

            annotation_list[gamma_plus_index] -= 1

            tot_ineq = orig
            for pos, value in enumerate(annotation_list):
                tot_ineq += value * candidate[pos]

            if tot_ineq < 0:
                resolutions.append((annotation_list, orig))

    return resolutions


def _dumb_hash(cg: Dict[str, GammaBarOptions]) -> str:
    keys = sorted(cg.keys())
    h = ""
    for k in keys:
        v = cg[k]
        h += "%s,%d,%d;" % (k, v.worker_type, v.num_popped)
    return h


def is_consistent(
    constraints: List[Constraint],
    car_gamma: Dict[str, CarGammaBarOptions],
    variable_mapping: Dict[str, int],
) -> bool:
    gb = pick_highest_prob(car_gamma)

    a_eq = []
    for i in range(len(variable_mapping)):
        row = [0.0] * len(variable_mapping)
        row[i] = 1.0
        a_eq.append(row)

    b_eq: List[float] = [0] * len(variable_mapping)
    for k, v in gb.items():
        b_eq[variable_mapping["gamma_plus_%s" % k]] = v[0]
        b_eq[variable_mapping["delta_gamma_%s" % k]] = v[1]

    a_ub = []
    for c in constraints:
        a_ub.append([-1 * d for d in c[0]])
    b_ub = [c[1] for c in constraints]

    res = linprog(
        [1.0] * len(variable_mapping), A_ub=a_ub, b_ub=b_ub, A_eq=a_eq, b_eq=b_eq
    )

    return res.status != 2


def is_ccvdc(
    stnu: STNU,
    car_gamma: Dict[str, GammaBarOptions],
    maximum_failure_risk: float,
    variable_mapping: Dict[str, int],
) -> Tuple[bool, float, GammaBar]:
    q: List[Tuple[float, int, List, Dict[str, GammaBarOptions]]] = []
    entry_count = 0
    heappush(q, (0, entry_count, [], car_gamma))
    visited_cg: Set[str] = set()
    while len(q) > 0:
        risk, _, constraints, cg = heappop(q)
        h = _dumb_hash(cg)
        if h in visited_cg:
            continue
        visited_cg.add(h)
        high_prob_gb = pick_highest_prob(cg)
        fixed, gamma = convert_to_fixed_delay(stnu, high_prob_gb)
        controllable, conflict = is_delay_controllable(fixed, gamma)
        if controllable:
            return True, risk, high_prob_gb
        for c in extract_resolutions(
            conflict, variable_mapping, stnu, gamma, high_prob_gb
        ):
            if not isinstance(c, list):
                c = [c]
            for constraint in c:
                if constraint in constraints:
                    continue
                new_constraints = copy(constraints)
                new_constraints.append(constraint)

                for k in cg.keys():
                    cg_copy: Dict[str, CarGammaBarOptions] = deepcopy(cg)
                    cand = cg_copy[k]
                    if not cand.has_next():
                        continue
                    p_success = (1.0 - risk) / cand.best_prob()
                    cg_copy[k].next_candidate()
                    p_success *= cand.best_prob()
                    while not is_consistent(new_constraints, cg_copy, variable_mapping):
                        if not cand.has_next():
                            p_success = 0
                            break
                        p_success /= cand.best_prob()
                        cg_copy[k].next_candidate()
                        p_success *= cand.best_prob()
                    cg_copy[k] = cand
                    if _dumb_hash(cg_copy) in visited_cg:
                        continue
                    if 1 - p_success <= maximum_failure_risk:
                        entry_count += 1
                        heappush(
                            q, (1 - p_success, entry_count, new_constraints, cg_copy)
                        )

    return False, 1.0, {}
