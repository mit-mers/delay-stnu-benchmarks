from random import randint, uniform
import time
from typing import Dict, List, Tuple

from src.custom_types import (
    GammaBarOptions,
    OptionsList,
    PSEUDO_INFINITY,
)
from src.stnu import STNU
from .car_rental import is_ccvdc

sampling_options: List[OptionsList] = [
    [(1.0, (PSEUDO_INFINITY, PSEUDO_INFINITY)), (0.9, (5.0, 3.0)), (0.5, (3.0, 0.0))],
    [(1.0, (PSEUDO_INFINITY, PSEUDO_INFINITY)), (0.95, (5.0, 3.0)), (0.9, (3.0, 0.0))],
    [(1.0, (PSEUDO_INFINITY, PSEUDO_INFINITY)), (0.99, (5.0, 3.0)), (0.9, (3.0, 0.0))],
]

mcc_options: OptionsList = [
    (1.0, (PSEUDO_INFINITY, PSEUDO_INFINITY)),
    (0.99, (3.0, 1.0)),
    (0.9, (1.0, 0.0)),
]

good_options = [(1.0, (0.0, 0.0))]


def generate_xeva_sampling_ccvdc_stnu(
    num_ev: int, num_activities: int, average_activity_time=40
) -> Tuple[STNU, Dict[str, int], Dict[str, GammaBarOptions]]:
    s = STNU()
    # track variables that will eventually be put in a LP
    var_map = {}
    # note that stnu will uppercase all activities, so make sure that the keys here are uppercased
    gamma_bar = {}

    all_start = "ALL-START"
    all_end = "ALL-END"
    limiting_consumable: float = average_activity_time * num_activities

    s.add_requirement_link(all_start, all_end, 0.0, limiting_consumable)

    for i in range(num_ev):
        for j in range(num_activities):
            if j == 0:
                s.add_requirement_link(
                    all_start, f"EV:SAMPLING:{i}:{j}", 0.0, PSEUDO_INFINITY
                )

            s.add_contingent_link(
                f"EV:SAMPLING:{i}:{j}",
                f"SBT:DECISION:{i}:{j}",
                uniform(5, 10),
                uniform(10, 15),
            )
            var_map[f"gamma_plus_SBT:DECISION:{i}:{j}"] = len(var_map)
            var_map[f"delta_gamma_SBT:DECISION:{i}:{j}"] = len(var_map)
            gamma_bar[f"SBT:DECISION:{i}:{j}"] = GammaBarOptions(
                sampling_options[randint(0, 2)]
            )

            lower_bound = uniform(0, 5)
            s.add_requirement_link(
                f"SBT:DECISION:{i}:{j}",
                f"MCC:RELAY:{i}:{j}",
                lower_bound,
                lower_bound + 3 + uniform(0, 8),
            )

            s.add_contingent_link(f"MCC:RELAY:{i}:{j}", f"EV:STOW:{i}:{j}", 0.0, 3.0)
            var_map[f"gamma_plus_EV:STOW:{i}:{j}"] = len(var_map)
            var_map[f"delta_gamma_EV:STOW:{i}:{j}"] = len(var_map)
            gamma_bar[f"EV:STOW:{i}:{j}"] = GammaBarOptions(mcc_options)

            s.add_requirement_link(f"EV:STOW:{i}:{j}", f"EV:TRAVERSE:{i}:{j}", 0.0, 3.0)

            if (j + 1) < num_activities:
                s.add_contingent_link(
                    f"EV:TRAVERSE:{i}:{j}",
                    f"EV:SAMPLING:{i}:{j+1}",
                    uniform(5, 7),
                    uniform(7, 10),
                )
                var_map[f"gamma_plus_EV:SAMPLING:{i}:{j+1}"] = len(var_map)
                var_map[f"delta_gamma_EV:SAMPLING:{i}:{j+1}"] = len(var_map)
                gamma_bar[f"EV:SAMPLING:{i}:{j+1}"] = GammaBarOptions(good_options)
            else:
                s.add_contingent_link(
                    f"EV:TRAVERSE:{i}:{j}",
                    f"EV:FINISH:{i}",
                    uniform(5, 7),
                    uniform(7, 10),
                )
                var_map[f"gamma_plus_EV:FINISH:{i}"] = len(var_map)
                var_map[f"delta_gamma_EV:FINISH:{i}"] = len(var_map)
                gamma_bar[f"EV:FINISH:{i}"] = GammaBarOptions(good_options)

                s.add_requirement_link(f"EV:FINISH:{i}", all_end, 0.0, PSEUDO_INFINITY)

    return s, var_map, gamma_bar


if __name__ == "__main__":
    for _ in range(100):
        for num_ev in range(1, 8):
            for num_activities in range(1, 8):
                (
                    test_stnu,
                    variable_mapping,
                    gamma_bar,
                ) = generate_xeva_sampling_ccvdc_stnu(num_ev, num_activities)

                start = time.time()
                results = is_ccvdc(test_stnu, gamma_bar, 0.50, variable_mapping)
                end = time.time()
                print(f"Number of EV: {num_ev}")
                print(f"Number of activities: {num_activities}")
                print(results)
                print(end - start)
                print("\n")
