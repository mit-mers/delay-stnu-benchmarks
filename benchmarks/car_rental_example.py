import time

from typing import Dict, Tuple
from random import randint, uniform

from .car_rental import is_ccvdc, CarGammaBarOptions
from src.stnu import STNU


def generate_car_rental_stnu(
    num_cars: int, trips_per_car: int
) -> Tuple[STNU, Dict[str, int], Dict[str, CarGammaBarOptions]]:
    s = STNU()
    var_map = {}
    gamma_bar = {}

    for c in range(num_cars):
        car = "CAR-%d" % c
        for t in range(trips_per_car):
            trip_start = "%s-START-%d" % (car, t)
            trip_end = "%s-END-%d" % (car, t)
            s.add_contingent_link(
                trip_start, trip_end, uniform(90, 105), uniform(220, 235)
            )
            var_map["gamma_plus_%s" % trip_end] = len(var_map)
            var_map["delta_gamma_%s" % trip_end] = len(var_map)
            worker = randint(1, 3)
            gamma_bar[trip_end] = CarGammaBarOptions(worker_type=worker)
            if t + 1 < trips_per_car:
                next_start = "%s-START-%d" % (car, t + 1)
                s.add_requirement_link(trip_end, next_start, 0, 30)

    return s, var_map, gamma_bar


if __name__ == "__main__":
    for num_trips in range(2, 26):  # [2, 5, 10, 15, 20, 25]:
        for _ in range(100):
            test_stnu, variable_mapping, gammabar = generate_car_rental_stnu(
                1, num_trips
            )

            start = time.time()
            results = is_ccvdc(test_stnu, gammabar, 0.15, variable_mapping)
            end = time.time()
            print("Number of trips: %d" % num_trips)
            print(results)
            print(end - start)
            print("\n")
