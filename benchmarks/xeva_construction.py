from random import randint, seed
from typing import Dict, Tuple
import numpy as np

from src.custom_types import (
    GammaBar,
    PSEUDO_INFINITY,
)
from src.delay import (
    convert_to_fixed_delay,
    is_delay_controllable,
)
from src.stnu import STNU

nil_obs_delay = (0.0, 0.0)

# this seed was used to generate the data in Section 8.5
# seed(1)
# np.random.seed(1)


def generate_XEVA_construction_STNU(
    num_ev: int, num_activities: int
) -> Tuple[STNU, Dict[str, int], Dict[str, GammaBar]]:
    # order is: vdc_delay_stnu, min_stnu, med_stnu, max_stnu, extended_stnu
    stnus = [STNU() for s in range(5)]
    gamma_bars = [{} for s in range(5)]

    all_starts = [f"ALL:START" for s in range(5)]
    all_ends = [f"ALL:END" for s in range(5)]
    limiting_consumable = num_activities * randint(50, 60)

    for s in range(5):
        # limiting consumable across the whole STNU
        stnus[s].add_requirement_link(
            all_starts[s], all_ends[s], 0, limiting_consumable
        )

    for i in range(num_ev):
        for j in range(num_activities):
            if j == 0:
                for s in range(5):
                    # the first activity for this EV should attach to the start of the STNU...
                    stnus[s].add_requirement_link(
                        all_starts[s], f"A:{i}:{j}", 0.0, PSEUDO_INFINITY
                    )

            # traverse
            lower_bound = 0
            upper_bound = randint(10, 20)
            for s in range(5):
                stnus[s].add_contingent_link(
                    f"A:{i}:{j}", f"B:{i}:{j}", lower_bound, upper_bound
                )
                gamma_bars[s][f"B:{i}:{j}"] = nil_obs_delay

            # install the dish
            lower_bound = 0
            upper_bound = randint(10, 15)
            for s in range(5):
                stnus[s].add_requirement_link(
                    f"B:{i}:{j}",
                    f"C:{i}:{j}",
                    lower_bound,
                    upper_bound,
                )

            # confirm the installation
            lower_bound = 0
            upper_bound = randint(lower_bound, 20)
            # upper_bound = randint(3, 10)
            exp_obs_delay = np.random.exponential(3)
            for s in range(5):
                min_obs_delay = 0.0
                max_obs_delay = 0.0
                my_upper_bound = upper_bound
                if s == 0:
                    # normal variable delay
                    min_obs_delay = 0.0
                    max_obs_delay = exp_obs_delay
                elif s == 1:
                    # min-fixed observation delay
                    min_obs_delay = max_obs_delay = 0.0
                elif s == 2:
                    # med-fixed observation delay
                    min_obs_delay = max_obs_delay = exp_obs_delay / 2
                elif s == 3:
                    # max-fixed observation delay
                    min_obs_delay = max_obs_delay = exp_obs_delay
                elif s == 4:
                    # combined observation and duration uncertainty
                    min_obs_delay = max_obs_delay = 0.0
                    my_upper_bound = upper_bound + exp_obs_delay

                gammabar_plus = max_obs_delay
                delta_gamma = max_obs_delay - min_obs_delay
                gamma_bars[s][f"D:{i}:{j}"] = (gammabar_plus, delta_gamma)
                stnus[s].add_contingent_link(
                    f"C:{i}:{j}",
                    f"D:{i}:{j}",
                    lower_bound,
                    my_upper_bound,
                )

            # let the next EV know they can confirm
            other_i = (i + 1) % num_ev
            other_j = j + 1 if other_i == 0 else j
            if j < num_activities - 1 and i < num_ev - 1:
                for s in range(5):
                    stnus[s].add_requirement_link(
                        f"D:{i}:{j}",
                        f"C:{other_i}:{other_j}",
                        0,
                        PSEUDO_INFINITY,
                    )

            # wrap up this activity
            if (j + 1) < num_activities:
                lower_bound = randint(0, 10)
                upper_bound = lower_bound + randint(4, 10)
                for s in range(5):
                    stnus[s].add_requirement_link(
                        f"D:{i}:{j}",
                        f"A:{i}:{j + 1}",
                        lower_bound,
                        upper_bound
                        if s != 4
                        else max(upper_bound - exp_obs_delay, upper_bound),
                    )
            else:
                for s in range(5):
                    # this is the last activity so wrap up the EVA
                    stnus[s].add_requirement_link(
                        f"D:{i}:{j}", all_ends[s], 0, PSEUDO_INFINITY
                    )

    return stnus, gamma_bars


if __name__ == "__main__":
    num_trials = 1000

    # the STNU is variable delay controllable and so is the approximation
    vdc_approx = [0 for _ in range(5)]
    # the STNU is variable delay controllable but the approximation is not
    vdc_not_approx = [0 for _ in range(5)]
    # the STNU is not variable delay controllable but the approximation is
    not_vdc_approx = [0 for _ in range(5)]
    # the STNU is not variable delay controllable and the approximation is not either
    not_vdc_not_approx = [0 for _ in range(5)]

    for i in range(1, num_trials + 1):
        if i % 25 == 0:
            print(f"Progress: {i:3} / {num_trials}")

        num_ev = 4
        num_activities = 5

        (
            test_stnus,
            gamma_bars,
        ) = generate_XEVA_construction_STNU(num_ev, num_activities)

        # variable delay STNU
        fixed, gamma = convert_to_fixed_delay(test_stnus[0], gamma_bars[0])
        is_vdc, _ = is_delay_controllable(fixed, gamma)

        for s in range(1, 5):
            approx_fixed, approx_gamma = convert_to_fixed_delay(
                test_stnus[s], gamma_bars[s]
            )
            approx_is_controllable, conflicts = is_delay_controllable(
                approx_fixed, approx_gamma
            )
            # STNU.draw(approx_fixed.as_edgelist(gamma_bars[s]))

            if is_vdc:
                if approx_is_controllable:
                    vdc_approx[s] += 1
                else:
                    # if s != 4 and len(conflicts) <= 5:
                    #     print(conflicts)
                    #     STNU.draw(approx_fixed.as_edgelist(gamma_bars[s]))
                    vdc_not_approx[s] += 1
            else:
                if approx_is_controllable:
                    not_vdc_approx[s] += 1
                else:
                    not_vdc_not_approx[s] += 1

    print("Min-fixed approximation")
    print(
        "{:<13} {:<13} {:<13} {:<13}".format(
            "VDC, Approx", "VDC, !Approx", "!VDC, Approx", "!VDC, !Approx"
        )
    )
    print(
        "{:<13} {:<13} {:<13} {:<13}".format(
            vdc_approx[1],
            vdc_not_approx[1],
            not_vdc_approx[1],
            not_vdc_not_approx[1],
        )
    )

    print("Med-fixed approximation")
    print(
        "{:<13} {:<13} {:<13} {:<13}".format(
            "VDC, Approx", "VDC, !Approx", "!VDC, Approx", "!VDC, !Approx"
        )
    )
    print(
        "{:<13} {:<13} {:<13} {:<13}".format(
            vdc_approx[2],
            vdc_not_approx[2],
            not_vdc_approx[2],
            not_vdc_not_approx[2],
        )
    )

    print("Max-fixed approximation")
    print(
        "{:<13} {:<13} {:<13} {:<13}".format(
            "VDC, Approx", "VDC, !Approx", "!VDC, Approx", "!VDC, !Approx"
        )
    )
    print(
        "{:<13} {:<13} {:<13} {:<13}".format(
            vdc_approx[3],
            vdc_not_approx[3],
            not_vdc_approx[3],
            not_vdc_not_approx[3],
        )
    )

    print("Elongated approximation")
    print(
        "{:<13} {:<13} {:<13} {:<13}".format(
            "VDC, Approx", "VDC, !Approx", "!VDC, Approx", "!VDC, !Approx"
        )
    )
    print(
        "{:<13} {:<13} {:<13} {:<13}".format(
            vdc_approx[4],
            vdc_not_approx[4],
            not_vdc_approx[4],
            not_vdc_not_approx[4],
        )
    )

    # max variability approximation is at index 4
    # percent_max_not_variable = vdc_not_approx[4] / (
    #     not_vdc_approx[4] + not_vdc_not_approx[4]
    # )
    # TODO: I don't think this is what this is
    # print(f"Max approx false negatives: {percent_max_not_variable:.2%}")
