from collections import defaultdict
import unittest
from src.custom_types import PSEUDO_INFINITY

from src.delay import is_delay_controllable
from src.stnu import STNU


class SanityCheckTestCases(unittest.TestCase):
    """
    These are test cases that seemed to identify/be indicative of implementation problems during STNU generation
    """

    def test_conflicts1(self):
        s = STNU()

        s.add_contingent_link("B", "C", 0, 10)
        s.add_requirement_link("C", "D", 0, 9)
        g = defaultdict(float)
        g["C"] = 1.7
        controllable, conflict = is_delay_controllable(s, g)
        self.assertTrue(controllable)

    def test_conflicts2(self):
        s = STNU()

        s.add_contingent_link("C0", "D0", 0, 3)
        s.add_contingent_link("D0", "A1", 1, 7)
        g = {"D0": 1.079, "A1": 0}
        controllable, conflict = is_delay_controllable(s, g)
        self.assertFalse(controllable)

    def test_conflicts3(self):
        s = STNU()

        s.add_contingent_link("C01", "D01", 0, 16)
        s.add_requirement_link("D01", "C11", 0, PSEUDO_INFINITY)

        s.add_contingent_link("C10", "D10", 0, 16)
        s.add_requirement_link("D10", "C01", 0, PSEUDO_INFINITY)
        s.add_requirement_link("D10", "A11", 0, 4)
        s.add_contingent_link("A11", "B11", 0, 20)
        s.add_requirement_link("B11", "C11", 0, 10)

        g = defaultdict(float)
        controllable, conflicts = is_delay_controllable(s, g)
        self.assertFalse(controllable)