from collections import defaultdict
import unittest
from random import seed
import numpy as np

from benchmarks.car_rental import pick_highest_prob
from src.delay import convert_to_fixed_delay, is_delay_controllable
from src.stnu import STNU

from benchmarks.xeva_construction import generate_XEVA_construction_STNU

seed(6)
np.random.seed(6)


class XEVAConstructionTestCases(unittest.TestCase):
    def test_generator(self):
        num_ev = 2
        num_activities = 1
        test_stnus, gamma_bars = generate_XEVA_construction_STNU(num_ev, num_activities)

        fixed, gamma = convert_to_fixed_delay(test_stnus[0], gamma_bars[0])
        is_vdc, _ = is_delay_controllable(fixed, gamma)

        for i in range(1, 3):
            fixed, gamma = convert_to_fixed_delay(test_stnus[i], gamma_bars[i])
            controllable, _ = is_delay_controllable(fixed, gamma)

            if is_vdc:
                self.assertTrue(controllable)

        # TODO: test the elongated STNU


class InconsistenciesTestCases(unittest.TestCase):
    def test_vdc(self):
        s = STNU()

        s.add_requirement_link("ALL:START", "ALL:END", 0.000000, 200.000000)
        s.add_requirement_link("ALL:START", "A:0:0", 0.000000, 10000.000000)
        s.add_requirement_link("ALL:START", "A:1:0", 0.000000, 10000.000000)

        s.add_requirement_link("A:0:0", "B:0:0", 0.000000, 13.000000)
        s.add_contingent_link("B:0:0", "C:0:0", 0.000000, 2.000000)
        s.add_requirement_link("C:0:0", "B:1:0", 0.000000, 10000.000000)
        s.add_requirement_link("C:0:0", "D:0:0", 0.000000, 11.000000)
        s.add_contingent_link("D:0:0", "A:0:1", 0.000000, 7.000000)

        s.add_requirement_link("A:0:1", "B:0:1", 0.000000, 7.000000)
        s.add_contingent_link("B:0:1", "C:0:1", 0.000000, 5.000000)
        s.add_requirement_link("C:0:1", "B:1:1", 0.000000, 10000.000000)
        s.add_requirement_link("C:0:1", "D:0:1", 0.000000, 4.000000)

        s.add_requirement_link("A:1:0", "B:1:0", 0.000000, 1.000000)
        s.add_contingent_link("B:1:0", "C:1:0", 0.000000, 4.000000)
        s.add_requirement_link("C:1:0", "B:0:1", 0.000000, 10000.000000)
        s.add_requirement_link("C:1:0", "D:1:0", 0.000000, 18.000000)
        s.add_contingent_link("D:1:0", "A:1:1", 0.000000, 12.000000)

        s.add_requirement_link("A:1:1", "B:1:1", 0.000000, 4.000000)
        s.add_contingent_link("B:1:1", "C:1:1", 0.000000, 0.000000)
        s.add_requirement_link("C:1:1", "D:1:1", 0.000000, 4.000000)

        s.add_requirement_link("D:0:1", "ALL:END", 0.000000, 10000.000000)
        s.add_requirement_link("D:1:1", "ALL:END", 0.000000, 10000.000000)

        gamma_bar = defaultdict(lambda: (0.0, 0.0))
        gamma_bar["C:0:0"] = (1.7962355886399695, 1.7962355886399695)
        gamma_bar["C:0:1"] = (1.4042968166435847, 1.4042968166435847)
        gamma_bar["C:1:0"] = (3.7097835209275005, 3.7097835209275005)
        gamma_bar["C:1:1"] = (0.25817257971443563, 0.25817257971443563)

        fixed, gamma = convert_to_fixed_delay(s, gamma_bar)
        is_vdc, _ = is_delay_controllable(fixed, gamma)

        self.assertTrue(is_vdc)

    def test_approx(self):
        # the above STNU represented as a max-fixed approximation
        s = STNU()

        s.add_requirement_link("ALL:START", "ALL:END", 0.000000, 200.000000)
        s.add_requirement_link("ALL:START", "A:0:0", 0.000000, 10000.000000)
        s.add_requirement_link("ALL:START", "A:1:0", 0.000000, 10000.000000)

        s.add_requirement_link("A:0:0", "B:0:0", 0.000000, 13.000000)
        s.add_contingent_link("B:0:0", "C:0:0", 0.000000, 2.000000)
        s.add_requirement_link("C:0:0", "B:1:0", 0.000000, 10000.000000)
        s.add_requirement_link("C:0:0", "D:0:0", 0.000000, 11.000000)
        s.add_contingent_link("D:0:0", "A:0:1", 0.000000, 7.000000)

        s.add_requirement_link("A:0:1", "B:0:1", 0.000000, 7.000000)
        s.add_contingent_link("B:0:1", "C:0:1", 0.000000, 5.000000)
        s.add_requirement_link("C:0:1", "B:1:1", 0.000000, 10000.000000)
        s.add_requirement_link("C:0:1", "D:0:1", 0.000000, 4.000000)

        s.add_requirement_link("A:1:0", "B:1:0", 0.000000, 1.000000)
        s.add_contingent_link("B:1:0", "C:1:0", 0.000000, 4.000000)
        s.add_requirement_link("C:1:0", "B:0:1", 0.000000, 10000.000000)
        s.add_requirement_link("C:1:0", "D:1:0", 0.000000, 18.000000)
        s.add_contingent_link("D:1:0", "A:1:1", 0.000000, 12.000000)

        s.add_requirement_link("A:1:1", "B:1:1", 0.000000, 4.000000)
        s.add_contingent_link("B:1:1", "C:1:1", 0.000000, 0.000000)
        s.add_requirement_link("C:1:1", "D:1:1", 0.000000, 4.000000)

        s.add_requirement_link("D:0:1", "ALL:END", 0.000000, 10000.000000)
        s.add_requirement_link("D:1:1", "ALL:END", 0.000000, 10000.000000)

        gamma_bar = defaultdict(lambda: (0.0, 0.0))
        gamma_bar["C:0:0"] = (1.7962355886399695, 0.0)
        gamma_bar["C:0:1"] = (1.4042968166435847, 0.0)
        gamma_bar["C:1:0"] = (3.7097835209275005, 0.0)
        gamma_bar["C:1:1"] = (0.25817257971443563, 0.0)

        fixed, gamma = convert_to_fixed_delay(s, gamma_bar)
        is_controllable, _ = is_delay_controllable(fixed, gamma)

        self.assertTrue(is_controllable)

    def test_vdc_after_convert_to_fixed(self):
        s = STNU()

        s.add_requirement_link("ALL:START", "ALL:END", 0.000000, 200.000000)
        s.add_requirement_link("ALL:START", "A:0:0", 0.000000, 10000.000000)
        s.add_requirement_link("ALL:START", "A:1:0", 0.000000, 10000.000000)

        s.add_requirement_link("A:0:0", "B:0:0", 0.000000, 13.000000)
        s.add_contingent_link("B:0:0", "C:0:0", 0.000000, 0.203764)
        s.add_requirement_link("C:0:0", "B:1:0", 1.796236, 10000.000000)
        s.add_requirement_link("C:0:0", "D:0:0", 1.796236, 11.000000)
        s.add_contingent_link("D:0:0", "A:0:1", 0.000000, 5.203764)

        s.add_requirement_link("A:0:1", "B:0:1", 0.000000, 7.000000)
        s.add_contingent_link("B:0:1", "C:0:1", 0.000000, 3.595703)
        s.add_requirement_link("C:0:1", "B:1:1", 1.404297, 10000.000000)
        s.add_requirement_link("C:0:1", "D:0:1", 1.404297, 4.000000)

        s.add_requirement_link("A:1:0", "B:1:0", 0.000000, 1.000000)
        s.add_contingent_link("B:1:0", "C:1:0", 0.000000, 0.290216)
        s.add_requirement_link("C:1:0", "B:0:1", 3.709784, 10000.000000)
        s.add_requirement_link("C:1:0", "D:1:0", 3.709784, 18.000000)
        s.add_contingent_link("D:1:0", "A:1:1", 0.000000, 8.290216)

        s.add_requirement_link("A:1:1", "B:1:1", 0.000000, 4.000000)
        s.add_contingent_link("B:1:1", "C:1:1", 0.000000, 0.000000)
        s.add_requirement_link("C:1:1", "D:1:1", 0.000000, 4.000000)

        s.add_requirement_link("D:0:1", "ALL:END", 0.000000, 10000.000000)
        s.add_requirement_link("D:1:1", "ALL:END", 0.000000, 10000.000000)

        gamma = {
            "A:0:1": 0.0,
            "A:1:1": 0.0,
            "C:0:0": 1.7962355886399695,
            "C:0:1": 1.4042968166435847,
            "C:1:0": 3.7097835209275005,
            "C:1:1": 10000.0,
        }

        controllable, _ = is_delay_controllable(s, gamma)
        self.assertTrue(controllable)

    def test_approx_after_convert_to_fixed(self):
        s = STNU()

        s.add_requirement_link("ALL:START", "ALL:END", 0.000000, 200.000000)
        s.add_requirement_link("ALL:START", "A:0:0", 0.000000, 10000.000000)
        s.add_requirement_link("ALL:START", "A:1:0", 0.000000, 10000.000000)

        s.add_requirement_link("A:0:0", "B:0:0", 0.000000, 13.000000)
        s.add_contingent_link("B:0:0", "C:0:0", 0.000000, 2.000000)
        s.add_requirement_link("C:0:0", "D:0:0", 0.000000, 11.000000)
        s.add_requirement_link("C:0:0", "B:1:0", 0.000000, 10000.000000)
        s.add_contingent_link("D:0:0", "A:0:1", 0.000000, 5.203764)

        s.add_requirement_link("A:0:1", "B:0:1", 0.000000, 7.000000)
        s.add_contingent_link("B:0:1", "C:0:1", 0.000000, 5.000000)
        s.add_requirement_link("C:0:1", "B:1:1", 0.000000, 10000.000000)
        s.add_requirement_link("C:0:1", "D:0:1", 0.000000, 4.000000)

        s.add_requirement_link("A:1:0", "B:1:0", 0.000000, 1.000000)
        s.add_contingent_link("B:1:0", "C:1:0", 0.000000, 4.000000)
        s.add_requirement_link("C:1:0", "B:0:1", 0.000000, 10000.000000)
        s.add_requirement_link("C:1:0", "D:1:0", 0.000000, 18.000000)
        s.add_contingent_link("D:1:0", "A:1:1", 0.000000, 8.290216)

        s.add_requirement_link("A:1:1", "B:1:1", 0.000000, 4.000000)
        s.add_contingent_link("B:1:1", "C:1:1", 0.000000, 0.000000)
        s.add_requirement_link("C:1:1", "D:1:1", 0.000000, 4.000000)

        s.add_requirement_link("D:0:1", "ALL:END", 0.000000, 10000.000000)
        s.add_requirement_link("D:1:1", "ALL:END", 0.000000, 10000.000000)

        gamma = {
            "A:0:1": 0.0,
            "A:1:1": 0.0,
            "C:0:0": 1.7962355886399695,
            "C:0:1": 1.4042968166435847,
            "C:1:0": 3.7097835209275005,
            "C:1:1": 0.25817257971443563,
        }

        controllable, _ = is_delay_controllable(s, gamma)
        self.assertTrue(controllable)
