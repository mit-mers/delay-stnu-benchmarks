from collections import defaultdict
import unittest

from src.custom_types import GammaBarOptions, PSEUDO_INFINITY
from src.delay import convert_to_fixed_delay, is_delay_controllable
from src.stnu import STNU
from benchmarks.car_rental import is_ccvdc


class FixedDelayTestCases(unittest.TestCase):
    def test_triangles(self):
        s = STNU()
        s.add_contingent_link("A", "B", 20, 40)
        s.add_requirement_link("B", "C", 30, 45)
        s.add_requirement_link("A", "C", 60, 75)
        controllable, _ = is_delay_controllable(s, defaultdict(float))
        self.assertTrue(controllable)

        s2 = STNU()
        s2.add_contingent_link("B", "C", 20, 40)
        s2.add_requirement_link("A", "B", 30, 45)
        s2.add_requirement_link("A", "C", 60, 75)
        controllable, _ = is_delay_controllable(s2, defaultdict(float))
        self.assertFalse(controllable)

        s3 = STNU()
        s3.add_contingent_link("B", "C", 20, 40)
        s3.add_requirement_link("A", "B", 15, 30)
        s3.add_requirement_link("A", "C", 60, 75)
        controllable, _ = is_delay_controllable(s3, defaultdict(float))
        self.assertFalse(controllable)

    def test_L(self):
        s4 = STNU()
        s4.add_contingent_link("A", "B", 20, 40)
        s4.add_requirement_link("B", "C", 30, 45)
        s4.add_requirement_link("A", "C", 60, 75)
        s4.add_requirement_link("D", "C", 15, 15)
        gamma = defaultdict(float)
        gamma["B"] = 20
        controllable, _ = is_delay_controllable(s4, gamma)
        self.assertTrue(controllable)

    def test_one_end_event(self):
        s5 = STNU()
        s5.add_contingent_link("A", "B", 20, 40)
        s5.add_requirement_link("B", "C", 30, 45)
        s5.add_requirement_link("A", "C", 60, 75)
        s5.add_requirement_link("D", "C", 15, 15)
        g = defaultdict(float)
        g["B"] = 40
        controllable, conflict = is_delay_controllable(s5, g)
        self.assertFalse(controllable)


class VariableDelayTestCases(unittest.TestCase):
    def test_coffee(self):
        """
        Coffee example from section 2
        """
        s = STNU()
        s.add_contingent_link("A", "B", 15, 30)
        s.add_requirement_link("B", "C", 20, 30)
        s.add_requirement_link("C", "D", 5, 5)
        s.add_requirement_link("A", "D", 0, 60)
        g = defaultdict(float)
        g["B"] = (15, 10)
        fixed, gamma = convert_to_fixed_delay(s, g)
        controllable, _ = is_delay_controllable(fixed, gamma)
        self.assertTrue(controllable)

    def test_xeva_construction(self):
        """
        Example EVA construction task from section 8 with 2 crew, 1 activities

        We need 30 mins/activity to be VDC, with 20 min "stagger" between EV
        """
        s = STNU()
        s.add_requirement_link("ALL_START", "ALL_END", 0, 50)
        s.add_requirement_link("ALL_START", "A:1:1", 0, PSEUDO_INFINITY)
        s.add_requirement_link("ALL_START", "A:2:1", 0, PSEUDO_INFINITY)

        s.add_requirement_link("A:1:1", "B:1:1", 0, 15)
        s.add_contingent_link("B:1:1", "C:1:1", 0, 20)
        s.add_requirement_link("C:1:1", "B:2:1", 0, PSEUDO_INFINITY)
        s.add_requirement_link("C:1:1", "D:1:1", 10, 20)

        # B:2:1 needs to be able to be scheduled 20 mins after B:1:1
        # we only have 15 mins here. the other 5 mins could come from ALL_START -> A:2:1
        s.add_requirement_link("A:2:1", "B:2:1", 0, 15)
        s.add_contingent_link("B:2:1", "C:2:1", 0, 20)
        s.add_requirement_link("C:2:1", "D:2:1", 10, 20)

        s.add_requirement_link("D:1:1", "ALL_END", 0, PSEUDO_INFINITY)
        s.add_requirement_link("D:2:1", "ALL_END", 0, PSEUDO_INFINITY)

        g = defaultdict(float)
        g["C:1:1"] = (3, 3)
        g["C:2:1"] = (3, 3)

        fixed, gamma = convert_to_fixed_delay(s, g)
        controllable, _ = is_delay_controllable(fixed, gamma)
        self.assertTrue(controllable)


class CCVDCTestCases(unittest.TestCase):
    def test_walkthrough(self):
        """
        Simplified example for the CCVDC walkthrough in section 6
        """
        s = STNU()
        s.add_contingent_link("A", "B", 15, 30)
        s.add_requirement_link("B", "C", 20, 30)
        gamma_bar_options = {"B": GammaBarOptions([(1.0, (15, 10))])}
        var_map = {"delta_gamma_B": 0, "gamma_plus_B": 0}
        results = is_ccvdc(s, gamma_bar_options, 0.99, var_map)
        self.assertTrue(results[0])


class ConvertToFixedDelayTestCases(unittest.TestCase):
    """
    Test each lemma from section 4
    """

    def test_lemma1(self):
        s = STNU()

        s.add_contingent_link("A", "B", 0, 10)
        gamma_bar = {"B": (1.0, 0)}

        fixed, gamma = convert_to_fixed_delay(s, gamma_bar)
        self.assertEqual(gamma["B"], 1.0)

    def test_lemma2(self):
        s = STNU()

        s.add_contingent_link("A", "B", 0, 10)
        gamma_bar = {"B": (PSEUDO_INFINITY, 0)}

        fixed, gamma = convert_to_fixed_delay(s, gamma_bar)
        self.assertEqual(gamma["B"], PSEUDO_INFINITY)

    def test_lemma3(self):
        s = STNU()

        s.add_contingent_link("A", "B", 0, 1)
        gamma_bar = {"B": (2, 2)}

        fixed, gamma = convert_to_fixed_delay(s, gamma_bar)
        self.assertEqual(gamma["B"], PSEUDO_INFINITY)

    def test_lemma4(self):
        s = STNU()

        a, b = 0, 2
        gammabar_plus = 1
        gammabar_minus = 0
        delta_gamma = gammabar_plus - gammabar_minus
        s.add_contingent_link("A", "B", a, b)
        gamma_bar = {"B": (gammabar_plus, delta_gamma)}

        fixed, gamma = convert_to_fixed_delay(s, gamma_bar)

        self.assertEqual(gamma["B"], 0)
        self.assertEqual(
            fixed.contingent_links.pop().bounds(),
            [a + gammabar_plus, b + gammabar_minus],
        )

    def test_lemma6(self):
        s = STNU()

        a, b = 0, 2
        u, v = 3, 4
        gammabar_plus = 1
        gammabar_minus = 0
        delta_gamma = gammabar_plus - gammabar_minus

        s.add_contingent_link("X", "C", a, b)
        s.add_requirement_link("C", "Z", u, v)
        gamma_bar = {"C": (gammabar_plus, delta_gamma)}

        fixed, gamma = convert_to_fixed_delay(s, gamma_bar)

        self.assertEqual(gamma["C"], 0)
        self.assertEqual(
            fixed.requirement_links.pop().bounds(),
            [u - gammabar_minus, v - gammabar_plus],
        )

    def test_corollary6point1(self):
        s = STNU()

        a, b = 0, 2
        u, v = 3, 4
        gammabar_plus = 1
        gammabar_minus = 0
        delta_gamma = gammabar_plus - gammabar_minus

        s.add_requirement_link("Z", "C", u, v)
        s.add_contingent_link("X", "C", a, b)
        gamma_bar = {"C": (gammabar_plus, delta_gamma)}

        fixed, gamma = convert_to_fixed_delay(s, gamma_bar)

        self.assertEqual(gamma["C"], 0)
        self.assertEqual(
            fixed.requirement_links.pop().bounds(),
            [u + gammabar_plus, v + gammabar_minus],
        )
