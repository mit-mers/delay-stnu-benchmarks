from typing import Callable, Dict, List, Tuple, Union
from collections import defaultdict
from copy import copy
from heapq import heappop, heappush
from scipy.optimize import minimize

from .custom_types import Gamma, GammaBar, PSEUDO_INFINITY
from .delay import convert_to_fixed_delay, is_delay_controllable
from .stnu import ContingentLink, Edge, STNU


Constraint = Tuple[List[float], float]


def orig_weight(edge: Edge, gamma_bar: GammaBar) -> float:
    weight = edge.weight

    for k, v in edge.annotations.items():
        tuple_index = 0
        if k.startswith("gamma_plus_"):
            node = k[len("gamma_plus_") :]
        elif k.startswith("delta_gamma_"):
            node = k[len("delta_gamma_") :]
            tuple_index = 1
        else:
            continue

        weight -= v * gamma_bar[node][tuple_index]

    return weight


def candidate_assignment(
    variable_mapping: Dict[str, int], gamma_bar: GammaBar
) -> List[float]:
    x = [0.0 for _ in variable_mapping.keys()]
    for k, v in variable_mapping.items():
        tuple_index = 0
        if k.startswith("gamma_plus_"):
            node = k[len("gamma_plus_") :]
        elif k.startswith("delta_gamma_"):
            node = k[len("delta_gamma_") :]
            tuple_index = 1
        else:
            continue
        x[v] = gamma_bar[node][tuple_index]
    return x


def extract_conflict(
    edges: List[Edge],
    variable_mapping: Dict[str, int],
    stnu: STNU,
    candidate_gamma: Gamma,
    gamma_bar: GammaBar,
) -> List[Union[Constraint, List[Constraint]]]:
    resolutions = []
    candidate = candidate_assignment(variable_mapping, gamma_bar)
    freq_list: List[float] = [0] * len(variable_mapping)
    total_weight = 0
    for e in edges:
        total_weight += orig_weight(e, gamma_bar)
        for k, v in e.annotations.items():
            freq_list[variable_mapping[k]] += v

    total = total_weight
    for i, val in enumerate(freq_list):
        total += val * candidate[i]

    if total < 0:
        resolutions.append((freq_list, total_weight))

    lower_indices = [
        i
        for i in range(len(edges))
        if edges[i].label is not None and edges[i].label.islower()
    ]
    for i in lower_indices:
        edge = edges[i]
        lower_label = edge.label.upper()
        link = [
            l
            for l in stnu.contingent_links
            if l.end == lower_label and isinstance(l, ContingentLink)
        ][0]
        gamma_plus_index = variable_mapping["gamma_plus_%s" % lower_label]
        delta_gamma_index = variable_mapping["delta_gamma_%s" % lower_label]
        if candidate_gamma[lower_label] == PSEUDO_INFINITY:
            l1_freq = [0] * len(variable_mapping)
            l1_freq[gamma_plus_index] = -1
            l1 = PSEUDO_INFINITY - candidate[gamma_plus_index] - 1

            l2_freq = [0] * len(variable_mapping)
            l2_freq[delta_gamma_index] = -1
            l2 = (
                link.max_duration() - link.min_duration() - candidate[delta_gamma_index]
            )

            if l1 < 0 and l2 < 0:
                resolutions.append(
                    [
                        (l1_freq, PSEUDO_INFINITY - 1),
                        (l2_freq, link.max_duration() - link.min_duration()),
                    ]
                )
            elif l1(candidate) < 0:
                resolutions.append((l1_freq, PSEUDO_INFINITY - 1))
            elif l2(candidate) < 0:
                resolutions.append((l2_freq, link.max_duration() - link.min_duration()))
        else:
            next_index = (i + 1) % len(edges)
            subpath_edges = [edge, edges[next_index]]
            total_weight = edge.weight + edges[next_index].weight
            while total_weight > candidate_gamma[lower_label]:
                next_index = (next_index + 1) % len(edges)
                subpath_edges.append(edges[next_index])
                total_weight += edges[next_index].weight

            orig = 0
            annotation_list: List[float] = [0] * len(variable_mapping)
            for s in subpath_edges:
                orig += orig_weight(s, gamma_bar)
                for k, v in s.annotations.items():
                    annotation_list[variable_mapping[k]] += v

            annotation_list[gamma_plus_index] -= 1

            tot_ineq = orig
            for pos, value in enumerate(annotation_list):
                tot_ineq += value * candidate[pos]

            if tot_ineq < 0:
                resolutions.append((annotation_list, orig))

    print("NUM RESOLUTIONS: %d" % len(resolutions))
    return resolutions


def find_minimal_window(
    constraints: List[Constraint],
    variable_mapping: Dict[str, int],
    obj_fun: Callable[[List[float]], float],
    old_gb: GammaBar,
) -> Tuple[float, GammaBar]:
    old_x = candidate_assignment(variable_mapping, old_gb)

    # import pdb; pdb.set_trace()
    print("\n")
    print(old_x)

    constraint_dicts = []
    for c in constraints:

        def x_constraint(x):
            start = c[1]
            for i, val in enumerate(c[0]):
                start += val * x[i]
            return start

        constraint_dicts.append(
            {
                "type": "ineq",
                "fun": x_constraint,
                "jac": lambda _: c[0],
            }
        )

    res = minimize(
        obj_fun,
        old_x,
        method="SLSQP",
        # method='trust-constr',
        constraints=constraint_dicts,
        bounds=[(0, PSEUDO_INFINITY) for _ in variable_mapping],
    )

    print(res.x)
    print("OBJ: %f" % obj_fun(res.x))
    print("\n")

    new_gb = {}
    for k, v in variable_mapping.items():
        tuple_index = 0
        if k.startswith("gamma_plus_"):
            node = k[len("gamma_plus_") :]
        elif k.startswith("delta_gamma_"):
            node = k[len("delta_gamma_") :]
            tuple_index = 1
        else:
            continue
        if node not in new_gb:
            new_gb[node] = (0, 0)
        old_tuple = new_gb[node]
        if tuple_index == 0:
            new_gb[node] = (res.x[v], old_tuple[1])
        else:
            new_gb[node] = (old_tuple[0], res.x[v])

    return obj_fun(res.x), new_gb


def is_ccvdc(
    stnu: STNU,
    gamma_bar: GammaBar,
    maximum_failure_risk: float,
    variable_mapping: Dict[str, int],
    obj_fun: Callable[[List[float]], float],
) -> Tuple[bool, float, GammaBar]:
    q: List[Tuple[float, int, List[Constraint], GammaBar]] = []
    entry_count = 0
    heappush(q, (0, entry_count, [], gamma_bar))
    while len(q) > 0:
        weight, _, constraints, gb = heappop(q)
        fixed, gamma = convert_to_fixed_delay(stnu, gb)
        controllable, conflict = is_delay_controllable(fixed, gamma)
        if controllable:
            return True, weight, gb
        for constraint in extract_conflict(conflict, variable_mapping, stnu, gamma, gb):
            new_constraints = copy(constraints)
            if not isinstance(constraint, list):
                constraint = [constraint]
            for c in constraint:
                if c not in new_constraints:
                    new_constraints.append(c)
            obj, new_gb = find_minimal_window(
                new_constraints, variable_mapping, obj_fun, gb
            )
            if obj <= maximum_failure_risk:
                entry_count += 1
                heappush(q, (obj, entry_count, new_constraints, new_gb))

    return False, 1, {}
