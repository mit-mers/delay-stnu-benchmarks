from typing import Dict, List, Tuple
from random import randint

# GammaBar is represented as (gamma^+, delta_gamma)

GammaPlus = float
DeltaGamma = float

Gamma = Dict[str, float]
GammaBar = Dict[str, Tuple[GammaPlus, DeltaGamma]]

PSEUDO_INFINITY = 10000.0

Option = Tuple[float, Tuple[GammaPlus, DeltaGamma]]
OptionsList = List[Option]


class GammaBarOptions:
    def __init__(
        self,
        options: OptionsList,
        worker_type=None,
    ):
        self.num_popped = 0
        self.options = options
        self.worker_type = worker_type
        if not worker_type:
            self.worker_type = randint(1, 3)

    def best_prob(self) -> float:
        return self.options[0][0]

    def best_gamma(self) -> Tuple[GammaPlus, DeltaGamma]:
        return self.options[0][1]

    def has_next(self) -> bool:
        return len(self.options) >= 2

    def next_candidate(self):
        self.num_popped += 1
        self.options = self.options[1:]

    def __copy__(self):
        candidate = type(self)(self.worker_type)
        for _ in range(self.num_popped):
            candidate.next_candidate()
        return candidate
