from typing import Dict, Optional, Set
from collections import defaultdict
import matplotlib
import networkx as nx


class STNU:
    def __init__(self):
        self.nodes: Set[str] = set()
        self.requirement_links: Set[Link] = set()
        self.contingent_links: Set[ContingentLink] = set()

    def add_requirement_link(
        self, start: str, end: str, min_duration: float, max_duration: float
    ):
        if min_duration > max_duration:
            raise Exception(
                f"Invalid requirement link: {start}->{end} [{min_duration}, {max_duration}]"
            )

        start = start.upper()
        end = end.upper()

        self.requirement_links.add(Link(start, end, min_duration, max_duration))

    def add_contingent_link(
        self, start: str, end: str, min_duration: float, max_duration: float
    ):
        if min_duration > max_duration:
            raise Exception(
                f"Invalid contingent link: {start}->{end} [{min_duration}, {max_duration}]"
            )

        start = start.upper()
        end = end.upper()

        self.contingent_links.add(
            ContingentLink(start, end, min_duration, max_duration)
        )

    def as_edgelist(self, gammabars={}):
        ret = []
        for l in self.requirement_links:
            ret.append((l.start, l.end, {"bounds": l.bounds()}))
        for l in self.contingent_links:
            gammabar = gammabars.get(l.end, (0, 0))
            (gammabar_plus, delta_gamma) = gammabar
            gammabar_minus = gammabar_plus - delta_gamma
            ret.append(
                (
                    l.start,
                    l.end,
                    {"bounds": f"{l.bounds()}; g=[{gammabar_minus}, {gammabar_plus}]"},
                )
            )
        return ret

    def draw(edgelist):
        G = nx.DiGraph()
        G.add_edges_from(edgelist)
        pos = nx.kamada_kawai_layout(G)
        nx.draw_kamada_kawai(G, with_labels=True, arrows=True)
        nx.draw_networkx_edge_labels(
            G, pos, edge_labels={(e[0], e[1]): e[2]["bounds"] for e in edgelist}
        )
        matplotlib.pyplot.show()


class Edge:
    def __init__(
        self, start: str, end: str, weight: float, label: Optional[str] = None
    ):
        self.start: str = start
        self.end: str = end
        self.weight: float = weight
        self.label: Optional[str] = label

        self.annotations: Dict[str, int] = defaultdict(int)

    def __repr__(self):
        return "<start: %s; end: %s; weight: %f, label: %s>" % (
            self.start,
            self.end,
            self.weight,
            self.label,
        )

    def increment_annotation(self, key: str, val: int):
        self.annotations[key] += val

    def get_annotation(self, key: str) -> int:
        return self.annotations.get(key)

    def is_lower_case(self) -> bool:
        return self.label is not None and self.label.islower()

    def is_upper_case(self) -> bool:
        return self.label is not None and self.label.isupper()

    def as_edge(self):
        return (self.start, self.end, {"bounds": self.bounds()})


class Link:
    def __init__(self, start: str, end: str, min_duration: float, max_duration: float):
        start = start.upper()
        end = end.upper()

        self.start = start
        self.end = end
        self._min_duration = min_duration
        self._max_duration = max_duration

        self.forward_edge = Edge(start, end, max_duration)
        self.reverse_edge = Edge(end, start, -min_duration)

    def __repr__(self):
        return (
            self.start,
            self.end,
            self.bounds(),
        )

    def bounds(self):
        return [self._min_duration, self._max_duration]

    def min_duration(self):
        return self._min_duration

    def set_min_duration(self, min_duration):
        self._min_duration = min_duration
        self.reverse_edge.weight = -min_duration

    def max_duration(self):
        return self._max_duration

    def set_max_duration(self, max_duration):
        self._max_duration = max_duration
        self.forward_edge.weight = max_duration

    def __copy__(self):
        return type(self)(self.start, self.end, self.min_duration, self.max_duration)


class ContingentLink(Link):
    def __init__(self, start: str, end: str, min_duration: float, max_duration: float):
        super(ContingentLink, self).__init__(start, end, min_duration, max_duration)

        start = start.upper()
        end = end.upper()

        # TODO: is this actually gammabar^+, gammabar^-?
        self.upper_edge = Edge(end, start, -max_duration, label=end.upper())
        self.lower_edge = Edge(start, end, min_duration, label=end.lower())

    def set_min_duration(self, min_duration):
        super(ContingentLink, self).set_min_duration(min_duration)
        self.lower_edge.weight = min_duration

    def set_max_duration(self, max_duration):
        super(ContingentLink, self).set_max_duration(max_duration)
        self.upper_edge.weight = -max_duration
