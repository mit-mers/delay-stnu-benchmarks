from collections import defaultdict
from copy import deepcopy
from heapdict import heapdict
from typing import Dict, List, Optional, Tuple, Union
import os

from numpy.random.mtrand import gamma

from .stnu import Edge, STNU
from .custom_types import Gamma, GammaBar, PSEUDO_INFINITY


def pprint(*args):
    if os.environ.get("DEBUG", False):
        print(*args)


def convert_to_fixed_delay(stnu: STNU, gamma_bar: GammaBar) -> Tuple[STNU, Gamma]:
    new_stnu = STNU()
    new_gamma: Gamma = {}

    req_links = deepcopy(stnu.requirement_links)
    cont_links = deepcopy(stnu.contingent_links)

    for l in cont_links:
        e = l.end
        a = l.min_duration()
        b = l.max_duration()

        gamma_plus = gamma_bar[e][0]
        delta_gamma = gamma_bar[e][1]
        gamma_minus = gamma_plus - delta_gamma

        if gamma_plus is PSEUDO_INFINITY or delta_gamma == 0:
            new_gamma[e] = gamma_plus
        elif b - a < delta_gamma:
            new_gamma[e] = PSEUDO_INFINITY
        else:
            l.set_min_duration(a + gamma_plus)
            l.set_max_duration(b + gamma_minus)
            l.forward_edge.increment_annotation("gamma_plus_%s" % e, -1)
            l.upper_edge.increment_annotation("gamma_plus_%s" % e, 1)
            new_gamma[e] = 0

            for l_out in req_links:
                if l_out.start != e:
                    continue
                [u, v] = l_out.bounds()
                l_out.set_min_duration(u - gamma_minus)
                l_out.set_max_duration(v - gamma_plus)
                l_out.reverse_edge.increment_annotation("delta_gamma_%s" % e, -1)
            for l_in in req_links:
                if l_in.end != e:
                    continue
                [u, v] = l_in.bounds()
                l_in.set_min_duration(u + gamma_plus)
                l_in.set_max_duration(v + gamma_minus)
                l_in.forward_edge.increment_annotation("delta_gamma_%s" % e, -1)

    new_stnu.requirement_links = req_links
    new_stnu.contingent_links = cont_links
    return new_stnu, new_gamma


def is_delay_controllable(stnu: STNU, gamma: Gamma) -> Tuple[bool, List[Edge]]:
    reverse_edge_list: Dict[str, List[Edge]] = defaultdict(list)
    contingent_lower_bounds = {}
    for l in stnu.requirement_links:
        reverse_edge_list[l.forward_edge.end].append(l.forward_edge)
        reverse_edge_list[l.reverse_edge.end].append(l.reverse_edge)
    for l in stnu.contingent_links:
        reverse_edge_list[l.forward_edge.end].append(l.forward_edge)
        reverse_edge_list[l.reverse_edge.end].append(l.reverse_edge)
        reverse_edge_list[l.upper_edge.end].append(l.upper_edge)
        reverse_edge_list[l.lower_edge.end].append(l.lower_edge)
        contingent_lower_bounds[l.end] = l.min_duration()

    novel: Dict[Edge, List[Edge]] = {}
    neg_nodes: List[str] = []
    for k, v in reverse_edge_list.items():
        for edge in v:
            if edge.weight < 0:
                neg_nodes.append(k)
                break

    while len(neg_nodes) > 0:
        s = neg_nodes[0]

        res, conf, _ = delay_dijkstra(
            reverse_edge_list, gamma, s, [s], neg_nodes, novel, contingent_lower_bounds
        )
        if not res:
            return False, expand_novel(conf, novel)

    return True, []


def is_dynamic_controllable(stnu: STNU) -> Tuple[bool, List[Edge]]:
    nil_gamma = defaultdict(lambda: 0)
    return is_delay_controllable(stnu, nil_gamma)


def delay_dijkstra(
    reverse_edge_list: Dict[str, List[Edge]],
    gamma: Gamma,
    start: str,
    call_stack: List[str],
    negative_nodes: List[str],
    novel: Dict[Edge, List[Edge]],
    contingent_lower_bounds,
) -> Tuple[bool, List[Edge], Optional[str]]:
    """
    See page 17 of:

    Bhargava, N., Muise, C., Vaquero, T., & Williams, B. (2018). Delay Controllability : Multi-Agent Coordination under Communication Delay. http://hdl.handle.net/1721.1/113340
    """
    if start in call_stack[1:]:
        return False, [], start
    pprint("\n\nSTARTING: %s\n" % start)

    label_preds: Dict[str, Tuple[Union[Edge, List[Edge]], bool]] = {}
    unlabel_preds: Dict[str, Tuple[Union[Edge, List[Edge]], bool]] = {}

    label_distance: Dict[str, float] = {start: 0}
    unlabel_distance: Dict[str, float] = {start: 0}

    # (node, label) -> weight
    q: Dict[Tuple[str, Optional[str]], float] = heapdict()
    for e in reverse_edge_list[start]:
        if e.weight < 0 and not e.is_lower_case():
            q[(e.start, e.label)] = e.weight
            if e.label is None:
                unlabel_distance[e.start] = e.weight
                unlabel_preds[e.start] = (e, False)
            else:
                label_distance[e.start] = e.weight
                label_preds[e.start] = (e, False)

    while len(q) > 0:
        key, weight = q.popitem()
        v, label = key
        pprint("POPPED: (%s, %s, %f)" % (v, label, weight))
        if weight >= 0:
            new_edge = Edge(v, start, weight)
            underlying_edges = []
            curr = v
            is_labeled = label is not None
            while curr != start:
                pred_list = label_preds if is_labeled else unlabel_preds
                prev_e, is_labeled = pred_list[curr]
                if not isinstance(prev_e, list):
                    prev_e = [prev_e]
                underlying_edges = underlying_edges + prev_e
                curr = prev_e[-1].end
                pprint(start, curr)
            reverse_edge_list[start].append(new_edge)
            novel[new_edge] = underlying_edges
            continue

        if v in negative_nodes:
            new_stack = [v] + call_stack
            pprint("\n\nRECURSING\n\n")
            result, conflict, end = delay_dijkstra(
                reverse_edge_list,
                gamma,
                v,
                new_stack,
                negative_nodes,
                novel,
                contingent_lower_bounds,
            )
            pprint("\n\nRETURNING TO %s\n\n" % start)
            if not result:
                if end is not None:
                    underlying_edges = []
                    curr = v
                    is_labeled = label is not None
                    while curr is not None:
                        pred_list = label_preds if is_labeled else unlabel_preds
                        prev_e, is_labeled = pred_list[curr]
                        if not isinstance(prev_e, list):
                            prev_e = [prev_e]
                        underlying_edges = underlying_edges + prev_e
                        curr = prev_e[-1].end
                        if curr == start:
                            curr = None
                    conflict = conflict + underlying_edges

                end = None if end == start else end
                return False, conflict, end

        for e in reverse_edge_list[v]:
            if e.weight < 0 or (e.is_lower_case() and e.label.upper() == label):
                continue
            w = e.weight + weight
            new_label = (
                None
                if label is not None and w > -1 * contingent_lower_bounds.get(label)
                else label
            )
            pred_list = unlabel_preds if new_label is None else label_preds
            dist = unlabel_distance if new_label is None else label_distance

            old_dist = dist.get(e.start)
            if old_dist is None or old_dist > w:
                pprint("ENQUEUING: (%s, %s, %f)" % (e.start, new_label, w))
                q[(e.start, new_label)] = w
                dist[e.start] = w
                pred_list[e.start] = (e, label is not None)

            lower_edges = [
                inc for inc in reverse_edge_list[e.start] if inc.is_lower_case()
            ]
            if len(lower_edges) > 0:
                lower = lower_edges[0]
                if e.weight < gamma[lower.label.upper()]:
                    lower_dist = dist.get(lower.start)
                    if lower_dist is None or lower_dist > w + lower.weight:
                        pprint(
                            "ENQUEUING (lower): (%s, %s, %f)"
                            % (lower.start, new_label, w + lower.weight)
                        )
                        q[(lower.start, new_label)] = w + lower.weight
                        dist[lower.start] = w + lower.weight
                        pred_list[lower.start] = ([lower, e], label is not None)

    negative_nodes.remove(start)
    return True, [], None


def expand_novel(edges: List[Edge], novel: Dict[Edge, List[Edge]]) -> List[Edge]:
    candidate = []
    for e in edges:
        if e not in novel:
            candidate.append(e)
        else:
            exp = novel[e]
            for e2 in exp:
                if not isinstance(e2, list):
                    e2 = [e2]
                candidate += expand_novel(e2, novel)
    return candidate
