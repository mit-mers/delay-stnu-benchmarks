#! /usr/bin/env python

import os.path
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sys


def format_data(data, dirname, names):
    for name in names:
        path = os.path.join(dirname, name)
        with open(path, "rb") as f:
            lines = f.readlines()
            line_number = 0

            is_dynamic = False
            is_strong = False
            is_delay = False

            current_index = 0
            dynamic_time = 0
            strong_time = 0
            delay_time = 0
            while line_number < len(lines):
                line = lines[line_number].decode("utf-8").strip()

                if line.startswith("Dynamic Output: "):
                    is_dynamic = line.endswith("T")
                if line.startswith("Strong Output: "):
                    is_strong = line.endswith("T")
                if line.startswith("Output: "):
                    is_delay = line.endswith("T")

                    sort_order = 0
                    controllability_type = "Fully Controllable"
                    if not is_strong:
                        if is_delay:
                            sort_order = 1
                            controllability_type = "Not SC, but DelC"
                        elif is_dynamic:
                            sort_order = 2
                            controllability_type = "Not DelC, but DC"
                        else:
                            sort_order = 3
                            controllability_type = "Uncontrollable"

                    data[controllability_type] += 1

                line_number += 1


def graph_controllabilities(folder_names):
    data = {
        "Fully Controllable": 0,
        "Not SC, but DelC": 0,
        "Not DelC, but DC": 0,
        "Uncontrollable": 0,
    }

    for folder_name in folder_names:
        for rootdir, dirs, files in os.walk(folder_name):
            format_data(data, rootdir, files)

    frame = pd.DataFrame(data, index=["A", "B", "C", "D"])

    sns.set_style("whitegrid")
    axes = sns.barplot(
        data=frame,
    )
    axes.set_title("Counts for different controllability checks")
    axes.set_ylabel("Count")
    axes.set_xlabel(folder_names[0])
    plt.show()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("You need to specify the folder name.")
        exit(1)
    graph_controllabilities(sys.argv[1:])
