"""
Parse benchmark results

Usage: ./analysis/parse.py path/to/dir
"""

import os.path
import pandas as pd
import seaborn as sns
import sys

from scipy.stats import ttest_rel


def calculate_speeds(data, dirname, names):
    base_folder = os.path.basename(dirname)
    for name in names:
        path = os.path.join(dirname, name)
        with open(path, "rb") as f:
            line_number = 0
            lines = f.readlines()
            while line_number < len(lines):
                line = lines[line_number]
                if "seconds of real time" in line:
                    data["time"].append(1000 * float(line.strip().split(" ")[0]))
                elif line.startswith("Conflict search: "):
                    value = line.strip().split(" ")[-1]
                    if value.endswith("d0"):
                        value = value[:-2]
                    data["Cost"].append(float(value))
                    data["Search Algorithm"].append("CDBFS")
                    data["Problem Size"].append(base_folder)
                elif line.startswith("Greedy search: "):
                    value = line.strip().split(" ")[-1]
                    if value.endswith("d0"):
                        value = value[:-2]
                    data["Cost"].append(data["Cost"][-1] / float(value))
                    # data['Cost'].append(float(value))
                    data["Search Algorithm"].append("LCRS")
                    data["Problem Size"].append(base_folder)
                elif line.startswith("Blind search: "):
                    value = line.strip().split(" ")[-1]
                    if value.endswith("d0"):
                        value = value[:-2]
                    data["Cost"].append(data["Cost"][-2] / float(value))
                    # data['Cost'].append(float(value))
                    data["Search Algorithm"].append("Blind")
                    data["Problem Size"].append(base_folder)
                line_number += 1


def parse_search_results(folder_name):
    data = {
        "time": [],
        "Cost": [],
        "Search Algorithm": [],
        "Problem Size": [],
    }

    for name in ["10", "20", "30", "40", "50"]:
        os.path.walk(os.path.join(folder_name, name), calculate_speeds, data)

    reordered_data = {
        "time": data["time"][1:],
        "Cost": data["Cost"][1:],
        "Search Algorithm": data["Search Algorithm"][1:],
        "Problem Size": data["Problem Size"][1:],
    }
    reordered_data["time"].append(data["time"][0])
    reordered_data["Cost"].append(data["Cost"][0])
    reordered_data["Search Algorithm"].append(data["Search Algorithm"][0])
    reordered_data["Problem Size"].append(data["Problem Size"][0])

    filtered_data = {
        "time": [d for i, d in enumerate(data["time"]) if i % 3 > 0],
        "Cost": [d for i, d in enumerate(data["Cost"]) if i % 3 > 0],
        "Search Algorithm": [
            d for i, d in enumerate(data["Search Algorithm"]) if i % 3 > 0
        ],
        "Problem Size": [d for i, d in enumerate(data["Problem Size"]) if i % 3 > 0],
    }
    # frame = pd.DataFrame(reordered_data)
    frame = pd.DataFrame(filtered_data)
    # print(frame)

    mean_1 = [d for i, d in enumerate(filtered_data["Cost"]) if i % 2 == 0]
    mean_2 = [d for i, d in enumerate(filtered_data["Cost"]) if i % 2 == 1]
    print(sum(mean_1) / len(mean_1))
    print(sum(mean_2) / len(mean_2))

    sns.set(font_scale=1.3, font="Times")
    sns.set_style("whitegrid")
    axes = sns.barplot(x="Problem Size", hue="Search Algorithm", y="Cost", data=frame)
    axes.set_title("Quality for Random Problems")
    axes.set_ylabel("Quality")
    sns.plt.legend(loc="lower left")
    # axes.set_ylabel('Runtime (milliseconds)')
    sns.plt.show()


def t_test_handle_folder(data, dirname, names):
    for name in names:
        path = os.path.join(dirname, name)
        times = []
        with open(path, "rb") as f:
            line_number = 0
            lines = f.readlines()
            while line_number < len(lines):
                line = lines[line_number]
                if line.startswith("Iteration #"):
                    time_is_for_conflict = True
                elif "seconds of real time" in line:
                    times.append(float(line.strip().split(" ")[0]))
                    # if time_is_for_conflict:
                    #     data['cda'].append(float(line.strip().split(' ')[0]))
                    #     time_is_for_conflict = False
                    # else:
                    #     data['greedy'].append(float(line.strip().split(' ')[0]))
                # elif line.startswith('Conflict search: '):
                #     value = line.strip().split(' ')[-1]
                #     if value.endswith('d0'):
                #         value = value[:-2]
                #     data['cda'].append(float(value))
                # elif line.startswith('Greedy search: '):
                #     value = line.strip().split(' ')[-1]
                #     if value.endswith('d0'):
                #         value = value[:-2]
                #     data['greedy'].append(float(value))

                line_number += 1
            for i in range(0, len(times), 3):
                data["cda"].append(times[i])
            for i in range(2, len(times), 3):
                data["greedy"].append(times[i])


def t_test(folder_name):
    data = {
        "greedy": [],
        "cda": [],
    }
    os.path.walk(folder_name, t_test_handle_folder, data)
    print(ttest_rel(data["greedy"], data["cda"]))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("You need to specify the folder name.")
        exit(1)
    parse_search_results(sys.argv[1])
    # t_test(sys.argv[1])
