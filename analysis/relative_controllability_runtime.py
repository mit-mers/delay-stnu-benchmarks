#! /usr/bin/env python

import os.path
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sys


def format_data(data, dirname, names):
    for name in names:
        path = os.path.join(dirname, name)
        with open(path, 'rb') as f:
            lines = f.readlines()
            line_number = 0

            is_dynamic = False
            is_strong = False
            is_delay = False

            current_index = 0
            dynamic_time = 0
            strong_time = 0
            delay_time = 0
            while line_number < len(lines):
                line = lines[line_number].decode('utf-8').strip()

                if line.endswith('seconds of real time'):
                    if current_index == 0:
                        dynamic_time = float(line.split(' ')[0])
                        current_index = 1
                    elif current_index == 1:
                        strong_time = float(line.split(' ')[0])
                        current_index = 2
                    else:
                        delay_time = max(0.000, float(line.split(' ')[0]))
                        current_index = 0

                if line.startswith('Dynamic Output: '):
                    is_dynamic = line.endswith('T')
                if line.startswith('Strong Output: '):
                    is_strong = line.endswith('T')

                if line.startswith('Output: '):
                    is_delay = line.endswith('T')

                    data['run_time'].append(dynamic_time)  # / delay_time)
                    data['run_time'].append(delay_time)
                    data['run_time'].append(strong_time)  # / delay_time)
                    data['Algorithm'].append('Dynamic')
                    data['Algorithm'].append('Delay')
                    data['Algorithm'].append('Strong')

                    sort_order = 0
                    controllability_type = 'Fully Controllable'
                    if not is_strong:
                        if is_delay:
                            sort_order = 1
                            controllability_type = 'Not SC, but DelC'
                        elif is_dynamic:
                            sort_order = 2
                            controllability_type = 'Not DelC, but DC'
                        else:
                            sort_order = 3
                            controllability_type = 'Uncontrollable'

                    data['sort_order'].append(sort_order)
                    data['sort_order'].append(sort_order)
                    data['sort_order'].append(sort_order)
                    data['Overall Controllability'].append(controllability_type)
                    data['Overall Controllability'].append(controllability_type)
                    data['Overall Controllability'].append(controllability_type)

                line_number += 1


def graph_controllabilities(folder_names):
    data = {
        'run_time': [],
        'Overall Controllability': [],
        'Algorithm': [],
        'sort_order': [],
    }

    for folder_name in folder_names:
        for rootdir, dirs, files in os.walk(folder_name):
            format_data(data, rootdir, files)

    frame = pd.DataFrame(data)

    sns.set_style("whitegrid")
    order = [
        'Fully Controllable',
        'Not SC, but DelC',
        'Not DelC, but DC',
        'Uncontrollable',
    ]
    axes = sns.barplot(
        x='Overall Controllability',
        hue='Algorithm',
        y='run_time',
        data=frame,
        order=order
    )
    axes.set_title('Runtime for different controllability checks')
    axes.set_ylabel('Runtime (seconds)')
    axes.set_xlabel(folder_names[0])
    plt.show()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('You need to specify the folder name.')
        exit(1)
    graph_controllabilities(sys.argv[1:])
