import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import pandas as pd
import sys


def generate_risk_graphs(filename):
    with open(filename, "r") as f:
        lines = [l.strip() for l in f.readlines()]
        i = 0

        data = {
            "Time (s)": [],
            "Failure Risk": [],
            "Number of Astronauts": [],
            "Number of Sampling Tasks": [],
            "Total Iterations": [],
        }

        while i < len(lines):
            line = lines[i]

            if line.startswith("Number of EV: "):
                num_ev = int(line.split(" ")[-1])
                i += 1
                line = lines[i]

                if line.startswith("Number of activities: "):
                    num_activities = int(line.split(" ")[-1])
                    i += 1
                    line = lines[i]

                    if "True" in line:
                        data["Failure Risk"].append(float(line.split(", ")[1]))
                        data["Number of Astronauts"].append(num_ev)
                        data["Number of Sampling Tasks"].append(num_activities)
                        data["Total Iterations"].append(num_ev ** num_activities)
                        i += 1
                        line = lines[i]
                        data["Time (s)"].append(float(line))

            i += 1

        frame = pd.DataFrame(data)
        fig = plt.figure()

        ax = fig.add_subplot(111, projection="3d")
        ax.scatter(
            frame["Number of Astronauts"],
            frame["Number of Sampling Tasks"],
            frame["Failure Risk"],
        )
        ax.view_init(-45, 15)

        ax.set_xlabel("Number of Astronauts", rotation=150)
        ax.set_ylabel("Number of Sampling Tasks")
        ax.set_zlabel("Failure Risk", rotation=60)
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.view_init(30, -135)
        plt.show()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("You need to specify the file name.")
        exit(1)
    generate_risk_graphs(sys.argv[1])
